package homework16;

public class MyLinkedList<T> implements MyList<T> {
    private Node first;
    private Node last;
    private int size = 0;

    @Override
    public T get(int index) {
        return getNode(index).value;
    }

    @Override
    public boolean add(T human) {
        if (size == 0) {
            first = new Node(null, human, null);
            last = first;
        } else {
            Node secondLast = last;
            last = new Node(secondLast, human, null);
            secondLast.next = last;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(T human) {
        int index = findElement(human);
        if (index != -1) {
            return removeAt(index);
        }
        return false;
    }

    @Override
    public boolean removeAt(int index) {
        Node node = getNode(index);
        Node nodeNext = node.next;
        Node nodePrevious = node.previous;
        if (nodeNext != null) {
            nodeNext.previous = nodePrevious;
        } else {
            last = nodePrevious;
        }
        if (nodePrevious != null) {
            nodePrevious.next = nodeNext;
        } else {
            first = nodeNext;
        }
        size--;
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public boolean add(T human, int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();

        }
        if (index == size) {
            return add(human);

        }

        Node nodeNext = getNode(index);
        Node nodePrevious = nodeNext.previous;
        Node newNode = new Node(nodePrevious, human, nodeNext);
        nodeNext.previous = newNode;
        if (nodePrevious != null) {
            nodePrevious.next = newNode;
        } else {
            first = newNode;
        }
        size++;
        return true;
    }

    @Override
    public boolean contains(T human) {
        return findElement(human) != -1;
    }

    private class Node {
        private Node previous;
        private T value;
        private Node next;

        public Node(Node previous, T value, Node next) {
            this.previous = previous;
            this.value = value;
            this.next = next;
        }
    }

    private Node getNode(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = first;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    private int findElement(T human) {
        Node node = first;
        for (int i = 0; i < size; i++) {
            if (node.value.equals(human)) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }
}
