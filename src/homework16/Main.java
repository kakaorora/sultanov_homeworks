package homework16;

public class Main {
    public static void main(String[] args) {
        MyArrayList<Human> myArrayList = new MyArrayList<>();
        Human human = new Human("Данил", 30, 70);
        Human human1 = new Human("Филат", 29, 86);
        Human human2 = new Human("Булат", 24, 90);
        Human human3 = new Human("Азат", 25, 110);
        Human human4 = new Human("Шамиль", 50, 65);
        Human human5 = new Human("Игорь", 44, 80);

        MyList<Human> myList = new MyLinkedList<>();

        myList.add(human);
        System.out.println("Получаем элемент по индексу в LinkedList");
        System.out.println(myList.get(0).toString());
        myArrayList.add(human1);
        myArrayList.add(human2);
        myArrayList.add(human3);
        myArrayList.add(human4);
        myArrayList.add(human5);
        System.out.println("Получине элемента по индеку в ArrayList");
        System.out.println(myArrayList.get(3));
        System.out.println("Добавление эдемента в ArrayList по индексу");
        myArrayList.add(human, 3);
        System.out.println("Проверка что элемент встал на этот индекс в ArrayList");
        System.out.println(myArrayList.get(3));
        System.out.println(myArrayList.get(0).toString());
        System.out.println(myArrayList.get(2).toString());


    }
}

