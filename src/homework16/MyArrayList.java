package homework16;

import java.util.Arrays;
import java.util.Objects;

public final class MyArrayList<T> implements MyList<T> {
    private T[] array;
    private int size = 0;

    public MyArrayList() {
        this.array = (T[]) new Object[10];
    }

    @Override
    public T get(int index) {
        chekIndex(index);
        return (T) array[index];
    }

    @Override
    public boolean add(T human) {
        increaseArray();
        array[size] = human;
        size++;
        return true;
    }

    @Override
    public boolean remove(T human) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(human)) {
                return removeAt(i);
            }
        }
        return false;
    }

    @Override
    public boolean removeAt(int index) {
        chekIndex(index);
        if (size - 1 - index >= 0) {
            System.arraycopy(array, index + 1, array, index, size - 1 - index);
        }
        size--;
        return true;
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        size = 0;

    }

    @Override
    public boolean add(T human, int index) {
        increaseArray();
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = human;
        size++;
        return true;

    }

    @Override
    public boolean contains(T human) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(human)) {
                return true;
            }
        }
        return false;

    }

    private void chekIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void increaseArray() {
        if (size >= array.length) {
            array = Arrays.copyOf(array, array.length * 2);

        }
    }


}
