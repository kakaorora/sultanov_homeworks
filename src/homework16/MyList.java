package homework16;

public interface MyList<T> {
    T get(int index);

    boolean add(T human);

    boolean remove(T human);

    boolean removeAt(int index);

    int size();

    void clear();

    boolean add(T human, int index);

    boolean contains(T human);

}

