create table artist (
                        id serial primary key,
                        artist_name varchar(20),
                        cost integer check (cost >= 0 and cost <= 50),
                        songs integer check (songs >= 0)
);

insert into artist (artist_name, cost, songs) values ('Tame Impala', 5, 2);
insert into artist (artist_name, cost, songs) values ('Pond', 5, 1);
insert into artist (artist_name, cost, songs) values ('The Kount', 7, 3);
insert into artist (artist_name, cost, songs) values ('Bones', 4, 2);
insert into artist (artist_name, cost, songs) values ('Sirotkin', 0, 50);
insert into artist (artist_name, cost, songs) values ('Ришат Тухватуллин', 4, 10);


create table buyer (
                       id serial primary key,
                       name varchar(30)
);

insert into buyer (name) values ('Игорь Султанов');
insert into buyer (name) values ('Артур Малыгин');
insert into buyer (name) values ('Альберт Гафуров');
insert into buyer (name) values ('Кит Шэпот');
insert into buyer (name) values ('Азат Хаерттинов');
insert into buyer (name) values ('Инна Солдатова');


create table stream_order (
                              artist_id integer,
                              buyer_id integer,
                              foreign key (artist_id) references artist (id), --внешний ключ
                              foreign key (buyer_id) references buyer (id), --внешний ключ
                              date varchar(10),
                              songs_count integer check (songs_count > 0)
);

insert into stream_order (artist_id, buyer_id, date, songs_count) values (2, 1, '13.05.2010', '3');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (1, 2, '27.02.2011', '2');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (3, 2, '09.11.2016', '4');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (5, 3, '13.05.2010', '10');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (5, 3, '13.05.2010', '20');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (6, 4, '25.12.2020', '1');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (6, 5, '13.11.2019', '1');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (4, 5, '26.12.2020', '2');
insert into stream_order (artist_id, buyer_id, date, songs_count) values (3, 6, '11.09.2021', '5');


--получить ценность песен Sirotkin
select cost as Sirotkin_cost from artist where artist_name = 'Sirotkin';

--получить кол-во заказов, сделанных 13.11.2019
select count (*) as orders_count from stream_order where date = '13.11.2019';

--получить имена пользователей, которые купили песни Ришата Тухватуллина
select name as Tatar_pop_listeners from buyer where buyer.id in
                                                     (select buyer_id from stream_order where stream_order.artist_id in
                                                                                              (select id from artist where artist_name = 'Ришат Тухватуллин'));

--получить товары, купленные Инной Солдатовой
select artist_name from artist where artist.id in
                                     (select artist_id from stream_order where stream_order.buyer_id in
                                                                               (select id from buyer where buyer.name = 'Инна Солдатова'));
