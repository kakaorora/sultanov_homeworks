package OOP;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();
        users.stream()
                .map(user -> user.getName() + " " + user.getAge() + " " + user.isWorker())
                .forEach(System.out::println);
        List<User> userAge = usersRepository.findByAge(26);
        System.out.println();
        userAge.stream()
                .map(User::getName)
                .forEach(System.out::println);
        System.out.println();
        List<User> userIsWork = usersRepository.findByIsWorkerIsTrue();
        userIsWork.stream()
                .map(User::getName)
                .forEach(System.out::println);
        User user = usersRepository.findById(2);
        user.setName("Денис");
        user.setAge(28);
        usersRepository.update(user);

    }

}
